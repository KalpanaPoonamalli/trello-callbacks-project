const fs = require("fs");
const path = require('path');


function callback4(callback){

    setTimeout(() => {

        const boardName = "Thanos" ;
        const boardsFilePath = path.join(__dirname, './boards.json')
        const listFilePath = path.join(__dirname, './lists.json');
        const cardsFilePath = path.join(__dirname, './cards.json')
    
        fs.readFile( boardsFilePath, 'utf-8', function(err, data){
            if(err){
                callback(err);
            } else {
    
                //console.log(data);

                const parsingBoardsData = JSON.parse(data);
                const findingBoard = parsingBoardsData.find(board => board.name === boardName)
                // console.log(findingBoard)

                console.log("Thanos Boards Information: ", findingBoard);

                const boardID = findingBoard.id;
                // console.log(boardID)

                fs.readFile(listFilePath, "utf-8", function(err, data){
                    if(err){
                        callback(err);
                    } else {

                        // console.log(data)

                        const parsingListsData = JSON.parse(data);
                        for (let key in parsingListsData){
                            // console.log(key)
                            if (key === boardID){
                                // console.log(key)

                                console.log("All the list for the Thanos Board: ", parsingListsData[key])

                               const findingMind = parsingListsData[key].find(board => board.name === "Mind");
                               console.log(findingMind);  
                               const mindBoardId = findingMind.id
                               console.log(mindBoardId)

                               fs.readFile(cardsFilePath, "utf-8", function(err, data){
                                if(err){
                                    callback(err);
                                } else {
                                    // console.log(data);

                                    const parsingCardsData = JSON.parse(data)
                                   // console.log(parsingCardsData);

                                    for (let key in parsingCardsData){
                                        // console.log(key)
                                        if(key === mindBoardId){
                                            // console.log(key)

                                            console.log("All cards for the Mind List: ", parsingCardsData[key]);
                                        }
                                    }
                                }
                               })
                            }
                        }
                    }
                })
            };
       });

    }, 2 * 1000); 

}



module.exports = callback4;