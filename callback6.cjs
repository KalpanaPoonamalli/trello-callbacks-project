const fs = require("fs");
const path = require('path');


function callback6(callback){

    setTimeout(() => {

        const boardName = "Thanos" ;
        const boardsFilePath = path.join(__dirname, './boards.json')
        const listFilePath = path.join(__dirname, './lists.json');
        const cardsFilePath = path.join(__dirname, './cards.json')
    
        fs.readFile( boardsFilePath, 'utf-8', function(err, data){
            if(err){
                callback(err);
            } else {
    
                //console.log(data);

                const parsingBoardsData = JSON.parse(data);
                const findingBoard = parsingBoardsData.find(board => board.name === boardName)
                // console.log(findingBoard)

                console.log("Thanos Boards Information: ", findingBoard);

                const boardID = findingBoard.id;
                // console.log(boardID)

                fs.readFile(listFilePath, "utf-8", function(err, data){
                    if(err){
                        callback(err);
                    } else {

                        // console.log(data)

                        const parsingListsData = JSON.parse(data);
                        for (let key in parsingListsData){
                            // console.log(key)
                            if (key === boardID){
                                // console.log(key)

                               console.log("All the list for the Thanos Board: ", parsingListsData[key])

                               fs.readFile(cardsFilePath, "utf-8", function(err, data){
                                if(err){
                                    callback(err);
                                } else {
                                    // console.log(data);

                                    const parsingCardsData = JSON.parse(data)
                                   // console.log(parsingCardsData);

                                    console.log("All cards for the all List: ", parsingCardsData);
                                        
                                    
                                }
                               })
                            }
                        }
                    }
                })
            };
       });

    }, 2 * 1000); 

}



module.exports = callback6;