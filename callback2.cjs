const fs = require("fs");
const path = require("path");


function callback2(callback){
    setTimeout(() => {

        const boardID = 'mcu453ed';
        const listFilePath = path.join(__dirname, './lists.json');

        fs.readFile(listFilePath, "utf-8", (err, data) => {
            if(err){
                callback(err);
            } else {

                // console.log(data)
                const parsingListsData = JSON.parse(data);
                //console.log(parsingListsData)

                for (let key in parsingListsData){
                    //console.log(key)
                    if(key === boardID){
                        console.log("Total List of the Board: ", parsingListsData[key]);
                    }
                }
            }
        })

    }, 2 * 1000)
}


module.exports = callback2;