const fs = require("fs");
const path = require('path');


function callback1(callback){

    setTimeout(() => {

        const boardID = 'abc122dc';
        const boardsFilePath = path.join(__dirname, './boards.json')

        fs.readFile( boardsFilePath, 'utf-8', function(err, data){
            if(err){
                callback(err);
            } else {
    
                //console.log(data);

                const parsingBoardData = JSON.parse(data);
    
                const findingBoard = parsingBoardData.find(board => board.id === boardID)
                //console.log(findingBoard)

                if(!findingBoard){
                    callback(err);
                } else{
                    console.log("Board Information: ", findingBoard)
                    // return findingBoard;
                    // callback(findingBoard);
                }
            };
       });

    }, 2 * 1000); 

}



module.exports = callback1;

