const fs = require("fs");
const path = require('path');


function callback3(callback){

    setTimeout(() => {

        const listID = "jwkh245";
        const cardssFilePath = path.join(__dirname, './cards.json')

        fs.readFile( cardssFilePath, 'utf-8', function(err, data){
            if(err){
                callback(err);
            } else {
    
                //console.log(data);

                const parsingCardsData = JSON.parse(data);
    
                for (let key in parsingCardsData){
                    //console.log(key)
                    if (key === listID){
                        console.log("All cards that belongs to a particular list: ", parsingCardsData[key])
                    }
                } 
            };
       });

    }, 2 * 1000); 

}



module.exports = callback3;